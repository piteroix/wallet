package com.pawlak.wallet.repository;

import com.pawlak.wallet.entity.PasswordEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PasswordRepository extends JpaRepository<PasswordEntity, Integer> {
}
