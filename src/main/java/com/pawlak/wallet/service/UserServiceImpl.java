package com.pawlak.wallet.service;

import com.pawlak.wallet.entity.UserEntity;
import com.pawlak.wallet.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserEntity findByid(int id) {

        Optional<UserEntity> userEntityFromDB = userRepository.findById(id);

        UserEntity userEntity;

        if (userEntityFromDB.isPresent()) {
            userEntity = userEntityFromDB.get();
        } else {
            throw new RuntimeException("There is no user in repository with id: " + id);
        }
        return userEntity;
    }

    @Override
    public List<UserEntity> findAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public void saveUser(UserEntity user) {
        userRepository.save(user);
    }

    @Override
    public void deleteUserById(int id) {

        userRepository.deleteById(id);
    }
}
