package com.pawlak.wallet.service;

import com.pawlak.wallet.entity.PasswordEntity;

import java.util.List;

public interface PasswordService {

    PasswordEntity findByid(int id);

    List<PasswordEntity> findAllPasswords();

    void savePassword(PasswordEntity password);

    void deletePasswordById(int id);
}
