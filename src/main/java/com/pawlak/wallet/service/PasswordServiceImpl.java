package com.pawlak.wallet.service;

import com.pawlak.wallet.entity.PasswordEntity;
import com.pawlak.wallet.repository.PasswordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PasswordServiceImpl implements PasswordService {

    private PasswordRepository passwordRepository;

    @Autowired
    public PasswordServiceImpl(PasswordRepository passwordRepository) {
        this.passwordRepository = passwordRepository;
    }

    @Override
    public PasswordEntity findByid(int id) {
        Optional<PasswordEntity> passwordEntityFromDB = passwordRepository.findById(id);

        PasswordEntity passwordEntity;

        if (passwordEntityFromDB.isPresent()) {
            passwordEntity = passwordEntityFromDB.get();
        } else {
            throw new RuntimeException("There is no password in repository with id: " + id);
        }

        return passwordEntity;
    }

    @Override
    public List<PasswordEntity> findAllPasswords() {
        return passwordRepository.findAll();
    }

    @Override
    public void savePassword(PasswordEntity password) {
        passwordRepository.save(password);
    }

    @Override
    public void deletePasswordById(int id) {
        passwordRepository.deleteById(id);
    }
}
