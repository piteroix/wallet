package com.pawlak.wallet.service;

import com.pawlak.wallet.entity.UserEntity;

import java.util.List;

public interface UserService {

    UserEntity findByid(int id);

    List<UserEntity> findAllUsers();

    void saveUser(UserEntity user);

    void deleteUserById(int id);
}
