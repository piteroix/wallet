package com.pawlak.wallet.controller;

import com.pawlak.wallet.entity.UserEntity;
import com.pawlak.wallet.service.PasswordService;
import com.pawlak.wallet.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {

    private UserService userService;
    private PasswordService passwordService;

    public UserController(UserService userService, PasswordService passwordService) {
        this.userService = userService;
        this.passwordService = passwordService;
    }

    @GetMapping("/usersList")
    public String showUserList(Model model){
        List<UserEntity> users = userService.findAllUsers();
        model.addAttribute("users", users);
    return "users/userList";
    }

    @RequestMapping("/new")
    public String showNewUserPage(Model model) {
        UserEntity user = new UserEntity();
        model.addAttribute("user", user);

        return "users/newUser";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveProduct(@ModelAttribute("user") UserEntity user) {
        userService.saveUser(user);

        return "redirect:/";
    }

    @RequestMapping("/edit/{id}")
    public ModelAndView showEditUserPage(@PathVariable(name = "id") int id) {
        ModelAndView mav = new ModelAndView("users/editUser");
        UserEntity user = userService.findByid(id);
        mav.addObject("user", user);

        return mav;
    }

    @RequestMapping("/delete/{id}")
    public String deleteProduct(@PathVariable(name = "id") int id) {
        userService.deleteUserById(id);
        return "redirect:/";
    }
}
